package com.mbds.grails


import grails.gorm.transactions.Transactional
import javassist.NotFoundException
import org.bouncycastle.openssl.PasswordException

import javax.management.InstanceAlreadyExistsException

@Transactional
class UserService {

    User get(Serializable id) {
        return User.get(id)
    }

    List<User> list(Map args) {
        return User.list(args)
    }

    Long count() {
        return User.count
    }

    void delete(Serializable id) throws Exception {
        def user = User.get(id)
        if (user == null) throw new NotFoundException("Utilisateur non trouvé")
        user.getAuthorities().each {
            Role role ->
                UserRole.findByRoleAndUser(role, user).delete()
        }
        user.delete()
    }

    def checkPassword(params, user) throws Exception{
        if(user.username != params.username){
            if(!(User.countByUsername(params.username) > 0))
                user.username = params.username
            else
                throw new InstanceAlreadyExistsException("Ce nom d'utilisateur existe déjà")
        }
        if (params.password && params.conf_password)
            if (params.password == params.conf_password)
                user.password = params.password
            else
                throw new PasswordException("Les mots de passes ne correspondent pas")
        else if (params.password && !params.conf_password)
            throw new PasswordException("Le champ confirmation mot de passe ne doit pas être vide")
        else if (!params.password && params.conf_password)
            throw new PasswordException("Le champ mot de passe ne doit pas être vide")
    }

//    def checkUsernameExist(username) {
//        (User.countByUsername(username) > 0)
//    }


    User save(params) throws Exception {
        def user = new User()
        user.username = null
        checkPassword(params, user)
        user.save(failOnError: true)
        new UserRole(user: user, role: Role.get(params.role.id)).save(failOnError: true)
        return user
    }

    User update(params) throws Exception {
        def user = User.get(params.id)
        UserRole.removeAll(user)
        checkPassword(params, user)
        user.passwordExpired = params.passwordExpired ? true : false
        user.accountLocked = params.accountLocked ? true : false
        user.accountExpired = params.accountExpired ? true : false
        user.enabled = params.enabled ? true : false

        user.save(failOnError: true)

        UserRole.create(user, Role.get(params.role.id), true)
        return user
    }

    User patch(params) throws Exception {
        def user = User.get(params.id)
        if (user == null) return null
        if (params.username) user.username = params.username

        checkPassword(params, user)

        if (params.passwordExpired) user.passwordExpired = params.passwordExpired ? true : false
        if (params.accountLocked) user.accountLocked = params.accountLocked ? true : false
        if (params.accountExpired) user.accountExpired = params.accountExpired ? true : false
        if (params.enabled) user.enabled = params.enabled ? true : false

        user.save(failOnError: true)
        if (params.role && params.role.id) {
            UserRole.removeAll(user)
            UserRole.create(user, Role.get(params.role.id), true)
        }
        return user
    }

}