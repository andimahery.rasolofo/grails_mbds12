package com.mbds.grails

import grails.core.GrailsApplication

class FileService {

    GrailsApplication grailsApplication

    Illustration uploadIllustration(file) {
        def filename = org.apache.commons.lang.RandomStringUtils.random(9, true, true) + "." + file.getOriginalFilename().split("\\.")[1]
        def illustration = new Illustration(filename: filename)
        file.transferTo(new File(grailsApplication.config.annonces.illustrations.path + filename))
        return illustration
    }

    List<Illustration> uploadIllustrations(files) {
        def liste = []
        files.each {
            file ->
                liste.add(uploadIllustration(file))
        }
        return liste
    }

    def deleteIllustrations(List<Illustration> illustrations) {
        illustrations.each {
            deleteFile(it.filename)
        }
    }

    def deleteFile(filename) {
        def file = new File(grailsApplication.config.annonces.illustrations.path + filename)
        if (file.exists()) file.delete()
    }
}
