package com.mbds.grails

import grails.gorm.transactions.Transactional
import javassist.NotFoundException

@Transactional
class AnnonceService {
    def fileService

    Annonce get(Serializable id) {
        return Annonce.get(id)
    }

    List<Annonce> list(Map args) {
        return Annonce.list(args)
    }

    Long count() {
        return Annonce.count
    }

    void delete(Serializable id) throws Exception {
        def annonce = Annonce.get(id)
        if (annonce == null) throw new NotFoundException("Announce not found")
        def illustrations = annonce.getIllustrations()
        annonce.delete()
        illustrations.each {
            Illustration illustration ->
                fileService.deleteFile(illustration.getFilename())
        }
    }

    Annonce save(params, file) throws Exception {
        def annonce = new Annonce()
        def listIllustr = null
        try {
            annonce.title = params.title
            annonce.description = params.description
            annonce.price = !(params.price instanceof Double) ? Double.parseDouble(params.price) : params.price
            annonce.author = User.get(params.author.id)
            annonce.author.addToAnnonces(annonce)
            if (file && !file[0].isEmpty()) {
//            annonce.addToIllustrations(fileService.uploadIllustration(file))
                listIllustr = fileService.uploadIllustrations(file)
                listIllustr.each {
                    annonce.addToIllustrations(it)
                }
            }
            annonce.author.save()
        } catch (Exception e) {
            fileService.deleteIllustrations(listIllustr)
            throw e
        }

        return annonce
    }

    Annonce update(params, file) throws Exception {
        def annonce = null
        def listIllustr = null
        try {
            annonce = Annonce.get(params.id)
            if (annonce == null) return null
            annonce.title = params.title
            annonce.description = params.description
            annonce.price = Double.parseDouble(params.price)

            def authorOld = null
            if (annonce.authorId != params.author.id) {
                authorOld = User.get(annonce.authorId)
                authorOld.removeFromAnnonces(annonce)
            }

            annonce.author = User.get(params.author.id)
            annonce.author.addToAnnonces(annonce)

            if (params.list('fileToDel')) {
                def illsToDel = []
                params.list('fileToDel').each {
                    def illToDel = Illustration.get(it)
                    println(illToDel.filename)
                    annonce.removeFromIllustrations(illToDel)
                    illToDel.delete()
                    illsToDel.add(illToDel)
                }
                fileService.deleteIllustrations(illsToDel)
            }

            if (!file[0].isEmpty()) {
                listIllustr = fileService.uploadIllustrations(file)
                listIllustr.each {
                    annonce.addToIllustrations(it)
                }
            }

            authorOld.save()
            annonce.author.save()
        } catch (Exception e) {
            fileService.deleteIllustrations(listIllustr)
            throw e
        }

        return annonce
    }

    Annonce patch(params, file) throws Exception {
        def annonce = null
        def listIllustr = null
        try {
            annonce = Annonce.get(params.id)
            if (annonce == null) return null
            if (params.title) annonce.title = params.title
            if (params.description) annonce.description = params.description
            if (params.price) annonce.price = (!params.price instanceof Double) ? Double.parseDouble(params.price) : params.price

            def authorOld = null
            if (params.author && annonce.authorId != params.author.id) {
                authorOld = User.get(annonce.authorId)
                authorOld.removeFromAnnonces(annonce)
            }

            def authorId = annonce.authorId
            if (params.author && params.author.id) authorId = params.author.id

            annonce.author = User.get(authorId)
            annonce.author.addToAnnonces(annonce)

            if (params.fileToDel) {
                def illsToDel = []
                params.fileToDel.each {
                    def illToDel = Illustration.get(it)
                    println(illToDel.filename)
                    annonce.removeFromIllustrations(illToDel)
                    illToDel.delete()
                    illsToDel.add(illToDel)
                }
                fileService.deleteIllustrations(illsToDel)
            }

            if (file && !file[0].isEmpty()) {
                listIllustr = fileService.uploadIllustrations(file)
                listIllustr.each {
                    annonce.addToIllustrations(it)
                }
            }

            if (authorOld) authorOld.save()
            annonce.author.save()
        } catch (Exception e) {
            fileService.deleteIllustrations(listIllustr)
            throw e
        }

        return annonce
    }

}