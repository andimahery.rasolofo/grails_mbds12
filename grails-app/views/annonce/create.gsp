<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="disposition"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title>Créer une annonce</title>
</head>

<body>

<div class="container-fluid">

    <!-- Page Heading
        <h1 class="h3 mb-2 text-gray-800">Poster une annonce</h1>-->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-info">Créer une annonce</h6>
        </div>
        <g:hasErrors bean="${this.annonce}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.annonce}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <g:uploadForm controller="annonce" action="save" method="POST">
            <div class="card-body">
                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-4">Titre</label>
                    <input type="text" value="" name="title" class="form-control col-sm-6" required=""
                           placeholder="Titre..."
                           aria-label="Title" aria-describedby="basic-addon2">
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-4">Description</label>
                    <input type="text" value="" name="description" required=""
                           class="form-control col-sm-6"
                           placeholder="Description..."
                           aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-4">Prix
                        <span class="required-indicator">*</span>
                    </label>
                    <input class="form-control col-sm-6" type="number" name="price" required=""
                           value="" placeholder="Prix...."
                           required="" min="0.0">
                </div>
                <!--
                    <div class="input-group" style="margin-bottom: 15px;">
                        <label class="col-sm-4">Illustrations</label>
                        <a class="btn btn-success btn-sm" href="/projet/illustration/create?annonce.id="><i class="fas fa-plus-circle"></i>&nbsp;Ajouter Illustration</a>
                    </div>-->

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-4" for="file">Illustrations</label>
                    <input class="btn btn-outline-info btn-sm" type="file" name="files[]" id="file" multiple/>
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-4">Auteur
                        <span class="required-indicator">*</span>
                    </label>
                    <g:select class="form-control col-sm-6" required="" name="author.id"
                              from="${userList}"
                              optionKey="id" optionValue="username"/>
                </div>
                <fieldset class="buttons">
                    <g:submitButton name="create" class="btn btn-success"
                                    value="${message(code: 'default.button.create.label', default: 'Créer')}"/>
                </fieldset>
            </div>
        </g:uploadForm>
    </div>
</div>
</body>
</html>
