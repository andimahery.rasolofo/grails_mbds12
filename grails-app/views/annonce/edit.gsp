<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="disposition"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title>Modifier l'annonce</title>
</head>

<body>

<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold ">Modifier l'annonce</h6>
        </div>
        <g:hasErrors bean="${this.annonce}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.annonce}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <g:uploadForm controller="annonce" action="update" id="${annonce.id}">
            <g:hiddenField name="version" value="${this.annonce?.version}"/>

            <div class="card-body">
                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2">Titre</label>
                    <input type="text" value="${annonce.title}" name="title" class="form-control col-sm-6" required=""
                           placeholder="Titre..."
                           aria-label="Title" aria-describedby="basic-addon2">
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2">Description</label>
                    <input type="text" value="${annonce.description}" name="description" required=""
                           class="form-control col-sm-6"
                           placeholder="Description..."
                           aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2">Prix
                        <span class="required-indicator">*</span>
                    </label>
                    <input class="form-control col-sm-6" type="number" name="price" required=""
                           value="${annonce.price}"
                           required="" min="0.0">
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2">Illustrations</label>
                    <g:each in="${annonce.illustrations}" var="illustration" status="i">
                        <img src="${baseUrl + illustration.filename}" class="img-thumbnail" width="100" height="100"/>
%{--                        <input  class="d-inline-block" data-toggle="tooltip" data-placement="top" title="Cocher pour effacer" style="display: inline" type="checkbox" name=""/>--}%
                        <g:if test="${annonce.illustrations.size() > 1}">
                            <g:checkBox class="d-inline-block" data-toggle="tooltip" data-placement="top" title="Cocher pour effacer"
                                        style="display: inline; margin-left: -13px" name="fileToDel" value="${illustration.id}" checked="false"/>&nbsp;&nbsp;
                        </g:if>
                    </g:each>
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2" for="files"></label>
                    <input class ="btn btn-outline-info btn-sm" style="display: inline" type="file" id="files" name="files[]" multiple/>
                </div>

                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2">Auteur
                        <span class="required-indicator">*</span>
                    </label>
                    <g:select class="form-control col-sm-6" required="" placeholder="Auteur..." name="author.id"
                              from="${userList}" optionKey="id" optionValue="username" value="${annonce.author.id}" />
                </div>
                <fieldset class="buttons">
                    <input class="btn btn-success" type="submit"
                           value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                </fieldset>
            </div>
        </g:uploadForm>

    </div>
</div>

</body>
</html>
