<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="disposition" />
        <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}" />
        <title>Listes des annonces</title>
    </head>
    <body>
    <div class="container-fluid">
        <!-- DataTales Example -->
        <g:if test="${flash.message}">
            <div class="alert alert-success col-lg-5" role="status">${flash.message} &nbsp; &nbsp; &nbsp;<asset:image src="checked.png" class="float-right"></asset:image></div>
        </g:if>
        <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <h6 class="m-0 font-weight-bold col-sm-10">Liste des annonces</h6>
                <a href="create" class="btn btn-success btn-sm col-sm-1">
                    Ajouter  <i class="fas fa-plus"></i>
                </a>
            </div>
        </div>
            <div class="card-body " >
                <div class="table-responsive overflow-hidden"  >
                    <table class="table table-bordered"  >
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Description</th>
                            <th>Prix</th>
                            <th>Illustrations</th>
                            <th>Auteur</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${annonceList}" var="annonce">
                            <tr>
                                <th><a href="show/${annonce.id}" class="text-info">${annonce.title}</a></th>
                                <th>${annonce.description}</th>
                                <th>${annonce.price}</th>
                                <th>
                                    <g:each in="${annonce.illustrations}" var="illustration">
                                        <img src="${grailsApplication.config.annonces.illustrations.url + illustration.filename}" class="img-thumbnail" width="100" height="100"/>
                                    </g:each>
                               </th>
                                <th>${annonce.author.username}</th>
                                <th><a href="edit/${annonce.id}" class="btn btn-info">
                                    <i class="fas fa-edit"></i>
                                </a></th><th><a href="#" class="btn btn-danger" data-toggle="modal" data-target="#supprimer${annonce.id}">
                                    <i class="fas fa-trash"></i>
                                </a></th>
                            </tr>
                            <div class="modal fade" id="supprimer${annonce.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Voulez vous vraiment supprimer cette annonce ?</div>
                                        <div class="modal-footer">
                                            <form action="/projet/annonce/delete/${annonce.id}" method="post"><input type="hidden" name="_method" value="DELETE" id="_method">
                                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                                                <input type="submit" class="btn btn-danger" value="Oui">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </g:each>
                        </tbody>
                    </table>
                </div>
                <g:paginate total="${annonceCount ?: 0}" />
            </div>

        <div class="card-footer" >

        </div>
        </div>
    </div>

    </body>
</html>