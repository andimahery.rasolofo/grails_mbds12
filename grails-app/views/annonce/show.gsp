<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="disposition"/>
    <g:set var="entityName" value="${message(code: 'annonce.label', default: 'Annonce')}"/>
    <title>Détail de l'annonce</title>
</head>

<body>

<div class="container-fluid">
    <!-- Page Heading  <h1 class="h3 mb-2 text-gray-800">Detail de l'annonce</h1>  -->

    <!-- DataTales Example -->
    <g:if test="${flash.message}">
        <div class="alert alert-success col-lg-5" role="status">${flash.message} &nbsp; &nbsp; &nbsp;<asset:image src="checked.png" class="float-right"></asset:image></div>
    </g:if>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-truncate">Détail de l'annonce</h6>
        </div>

        <div class="card-body">
            <div class="row" style="margin-bottom: 15px;">
                <label class="col-sm-2 text-info">Titre :</label>
                <label class="col-sm-4">${annonce.title}</label>
            </div>

            <div class="row" style="margin-bottom: 15px;">
                <label class="col-sm-2 text-info">Description :</label>
                <label class="col-sm-4">${annonce.description}</label>
            </div>

            <div class="row" style="margin-bottom: 15px;">
                <label class="col-sm-2 text-info">Prix :</label>
                <label class="col-sm-4">${annonce.price}</label>
            </div>

            <div class="row" style="margin-bottom: 15px;">
                <label class="col-sm-2 text-info">Illustrations :</label>
                <g:each in="${annonce.illustrations}" var="illustration">
                    <img src="${grailsApplication.config.annonces.illustrations.url + illustration.filename}" class="img-thumbnail" width="100" height="100" />&nbsp;&nbsp;
                </g:each>
            </div>

            <div class="row" style="margin-bottom: 15px;">
                <label class="col-sm-2 text-info">Auteur :</label>
                <label class="col-sm-4">${annonce.author.username}</label>
            </div>
            <a href="/projet/annonce/edit/${annonce.id}" class="btn btn-info btn-sm">
                <i class="fas fa-edit"></i>&nbsp;Modifier</a>
            <a  class="btn btn-danger btn-sm" data-toggle="modal" data-target="#supprimer_annonce">
                <i class="fas fa-trash"></i>&nbsp;Supprimer</a>
            <div class="modal fade" id="supprimer_annonce" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Voulez vous vraiment supprimer cette annonce ?</div>
                        <div class="modal-footer">
                            <form action="/projet/annonce/delete/${annonce.id}" method="post"><input type="hidden" name="_method" value="DELETE" id="_method">
                                <button class="btn btn-light" type="button" data-dismiss="modal">Annuler</button>
                                <input type="submit" class="btn btn-danger" value="Oui">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
