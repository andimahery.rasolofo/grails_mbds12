<%--
  Created by IntelliJ IDEA.
  User: Mahery
  Date: 12/03/2021
  Time: 16:08
--%>

<html lang="en" class="no-js>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Se connecter</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<asset:link rel="icon" href="promotion.png" type="image/x-ico" />
<asset:stylesheet src="application.css"/>
</head>
<body class="bg-gradient-info">

<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0 bg-gray-100">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                    <div class="col-lg-1 p-4"></div>
                        <div class="col-lg-5"> <asset:image src="megaphone_a.png"  style="width:325px;height:325px;"  alt="Alert"/></div>

                        <div class="col-lg-5 p-4">
                            <br>
                            <h5 class="card-title text-center text">Coin Coin Admin</h5>
                            <g:if test='${flash.message}'>
                                <div class="alert alert-danger" role="alert">${flash.message}  <asset:image src="warning.png" alt="Alert"/></div>
                            </g:if>

                            <form class="form-signin" action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" autocomplete="off">
                                <div class="form-group">
                                    <label for="username">Email</label>
                                    <input type="text" class="form-control" name="${usernameParameter ?: 'username'}" id="username" autocapitalize="none"/>
                                </div>

                                <div class="form-group">
                                    <label for="password">Mot de passe</label>
                                    <input type="password" class="form-control" name="${passwordParameter ?: 'password'}" id="password"/>
                                    <i id="passwordToggler" title="toggle password display" onclick="passwordDisplayToggle()">&#128065;</i>
                                </div>

                                <button id="submit" class="btn btn-lg btn-info btn-block " type="submit">Se connecter <i class="fas fa-sign-in-alt"></i></button>
                                <hr class="my-4">
                            </form>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function (event) {
        document.forms['loginForm'].elements['username'].focus();
    });

    function passwordDisplayToggle() {
        var toggleEl = document.getElementById("passwordToggler");
        var eyeIcon = '\u{1F441}';
        var xIcon = '\u{2715}';
        var passEl = document.getElementById("password");
        if (passEl.type === "password") {
            toggleEl.innerHTML = xIcon;
            passEl.type = "text";
        } else {
            toggleEl.innerHTML = eyeIcon;
            passEl.type = "password";
        }
    }
</script>


</body>
</html>