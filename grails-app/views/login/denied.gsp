<%--
  Created by IntelliJ IDEA.
  User: Mahery
  Date: 21/03/2021
  Time: 13:05
--%>
<%--
  Created by IntelliJ IDEA.
  User: Mahery
  Date: 12/03/2021
  Time: 16:08
--%>

<html lang="en" class="no-js>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Accès Refusé 403</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <asset:link rel="icon" href="promotion.png" type="image/x-ico"/>
    <asset:stylesheet src="application.css"/>
</head>
<body class="bg-gradient-info">
<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0 bg-gray-100">
                    <div class="text-center">
                        <div class="error mx-auto" data-text="403">403</div>

                        <p class="lead text-gray-800 mb-5">Accès refusé</p>

                        <p class="text-gray-500 mb-0">Vous n'êtes pas autorisé à consulter cette page</p>
                        <a href="\projet\annonce\index">&larr; Revenir</a>
                    </div>
                </div>
                <div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>