<%--
  Created by IntelliJ IDEA.
  User: Mahery
  Date: 04/03/2021
  Time: 16:45
--%>

<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
    <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <asset:link rel="icon" href="promotion.png" type="image/x-ico"/>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <asset:stylesheet src="application.css"/>
    <g:layoutHead/>
</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info  sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">

            <div class="sidebar-brand-text mx-3"><asset:image src="promotion_32.png"
                                                              alt="Coin coin Logo"/>  Coin Coin Admin</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading  text-light">
            Gestion
        </div>
        <!-- Nav Item - Tables -->
        <li class="nav-item">
            <a class="nav-link" href="/projet/annonce/index">
                <asset:image src="promotion_icon.png" alt=""/>
                <span>Annonce</span></a>
        </li>


        <!-- Nav Item - Charts -->
        <li class="nav-item">
            <a class="nav-link" href="/projet/user/index">
                <asset:image src="user.png" alt=""/>
                <span>Utilisateur</span></a>
        </li>



        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>

    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>


                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                             aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                           placeholder="Search for..." aria-label="Search"
                                           aria-describedby="basic-addon2">

                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>


                    <div class="topbar-divider d-none d-sm-block"></div>

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><sec:loggedInUserInfo
                                    field='username'/></span>
                            <asset:image src="user.png" class="img-profile rounded-circle" alt=""/>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                             aria-labelledby="userDropdown">
                        <!--
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                            Profile
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                            Settings
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log
                        </a>

                        <div class="dropdown-divider"></div>
                         -->
                            <g:form controller="logout">
                                <button type="submit" class="dropdown-item"><asset:image src="logout.png"
                                                                                         alt=""/> Déconnecter</button>
                            </g:form>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <g:layoutBody/>

        </div>
        <!-- Footer -->

        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright &copy; AR / MJ</span>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- End of Footer -->

<asset:javascript src="application.js"/>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<g:javascript>
    $(document).ready(function () {
        $('#password, #confirm_password').on('keyup', function () {
            if ($('#password').val()){
                $("#confirm_password").prop('required',true);
            }else{
                $("#confirm_password").prop('required',false);
            }

            if ($('#password').val() && $('#confirm_password').val()) {
                if ($('#password').val() == $('#confirm_password').val()) {
                    $('#message').html('Les mots de passe correspondent').css('color', 'green');

                    // $('#btnModifMdp').prop('disabled', null);
                    // $("#btnModifMdp").removeAttr("disabled");
                   // $("#btnModifMdp").attr('disabled', 'false');
                    $('#btnModifMdp').show();

                } else{
                    $('#message').html('Les mots de passe ne correspondent pas').css('color', 'red');
                    //  $('#btnModifMdp').prop('disabled', true);
                   // $("#btnModifMdp").attr('disabled', 'disabled'); //disable
                    $('#btnModifMdp').hide();

                }

            } else {
                $('#message').html(' ');
            }

        });
    });


    $(document).ready( function () {

       // $.fn.dataTable.ext.classes.sTable = ' ';
        $.fn.dataTable.ext.classes.sNoFooter = ' ';
        $.fn.dataTable.ext.classes.sPageButton = 'step';
        $.fn.dataTable.ext.classes.sPageButtonActive = 'currentStep';
        $.fn.dataTable.ext.classes.sPageButtonDisabled = 'd-none';
        $('#dataTable').DataTable({
            "language": {
                "decimal":        "",
                "emptyTable":     "Aucune donnée disponible",
                "info": "Affichage de la page _PAGE_ sur _PAGES_",
                "infoEmpty": "Aucun résultat",
                "infoFiltered":   "",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu": "Afficher _MENU_ par page",
                "loadingRecords": "Chargement...",
                "processing":     "Processing...",
                "search":         "Recherche:",
                "zeroRecords":    "Aucun résultats trouvés",
                "paginate": {
                    "first":      "First",
                    "last":       "Last",
                    "next":       "Suivant",
                    "previous":   "Précedent"
                },
                "aria": {
                    "sortAscending":  ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                }
            }
        });
    } );
</g:javascript>

</body>
</html>
