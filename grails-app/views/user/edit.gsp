<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="disposition"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title>Modifier l'utilisateur</title>
</head>

<body>
<div class="container-fluid">
    <g:if test="${flash.message}">
        <div class="alert alert-success col-lg-4" role="status">${flash.message} &nbsp; &nbsp; <asset:image
                src="checked.png"></asset:image></div>
    </g:if>
    <g:if test="${flash.error}">
        <div class="alert alert-danger col-lg-5" role="status">${flash.error} &nbsp; &nbsp; <asset:image class="float-right"
                src="warning.png"></asset:image></div>
    </g:if>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold ">Modifier l'utilisateur</h6>
        </div>
        <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <g:form resource="${this.user}" method="PUT">
            <g:hiddenField name="version" value="${this.user?.version}"/>
            <div class="card-body">
                <div class="input-group required">
                    <label class="col-sm-2" for="username">Nom
                    </label><input type="text" name="username" value="${user.username}" required=""
                                   class="form-control col-sm-6" id="username">

                </div>
                <hr>
                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-2">Role
                    </label>
                    <g:select id="role" class="form-control col-sm-6" required="" name="role.id"
                              from="${rolelist}" optionKey="id" optionValue="authority" value="${userRole.role.id}"/>
                </div>
                <hr>
                <div class="input-group">
                    <label class="col-sm-2" for="password">Nouveau mot de passe :
                    </label><input type="password" name="password"  class="form-control col-sm-6" id="password">
                </div>
                <br>

                <div class="input-group">
                    <label class="col-sm-2" for="confirm_password">Confirmation mot de passe :
                    </label><input type="password" name="conf_password"  class="form-control col-sm-6" value="" id="confirm_password">
                </div>
                <small id='message' class="offset-2"></small>
                <hr>
                <div class="input-group required">
                    <label class="col-sm-3" for="passwordExpired">Mot de passe expiré</label>
                    <g:checkBox type="checkbox" name="passwordExpired" class=" col-sm-1" id="passwordExpired"
                                value="${user.passwordExpired}" checked="${user.passwordExpired}"/>
                    <label class="col-sm-2" for="accountLocked">Compte bloqué</label>
                    <g:checkBox type="checkbox" name="accountLocked" class="col-sm-1" id="accountLocked"
                                value="${user.accountLocked}" checked="${user.accountLocked}"/>
                </div>
                <hr>

                <div class="input-group">
                    <label class="col-sm-3" for="accountExpired">Compte expiré</label>
                    <g:checkBox type="checkbox" name="accountExpired" class=" col-sm-1" id="accountExpired"
                                value="${user.accountExpired}" checked="${user.accountExpired}"/>
                    <label class="col-sm-2" for="enabled">Activé</label>
                    <g:checkBox type="checkbox" class="col-sm-1"
                                name="enabled" id="enabled" value="${user.enabled}" checked="${user.enabled}"/>
                </div>
                <hr>

                <div class="input-group">
                    <label class="col-sm-4">Liste des Annonces :</label>

                    <div class="col-sm-3"><a href="/projet/annonce/create?user.id=1" class="btn btn-success"><i
                            class="fas fa-plus-circle"></i>&nbsp;Ajouter une annonce</a></div>
                </div>
                <br>

                <div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr class="info">
                            <th>Titre</th>
                            <th>Description</th>
                            <th>Prix</th>
                            <th>Illustrations</th>
                            <th>Auteur</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${annonceList}" var="annonce">
                            <tr>
                                <th>${annonce.title}</th>
                                <th>${annonce.description}</th>
                                <th>${annonce.price}</th>
                                <th>
                                    <g:each in="${annonce.illustrations}" var="illustration">
                                        <img src="${grailsApplication.config.annonces.illustrations.url + illustration.filename}"
                                             class="img-thumbnail" width="100" height="100"/>
                                    </g:each>
                                </th>
                                <th>${annonce.author.username}</th>
                                <th><a href="/projet/annonce/show/${annonce.id}" class="btn btn-outline-warning">
                                    Voir détail
                                </th>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
                <br>
                <fieldset class="buttons">
                    <input class="btn btn-success" type="submit"
                           value='Modifier'/>
                </fieldset>
            </div>
        </g:form>
    </div>
</div>
</body>
</html>
