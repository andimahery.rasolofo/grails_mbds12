<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="disposition"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>

<div class="container-fluid">

    <!-- Page Heading
    <h1 class="h3 mb-2 text-gray-800">Poster une annonce</h1>-->

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"><h6 class="m-0 font-weight-bold ">Ajouter un utilisateur</h6></h6>
        </div>
        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${this.user}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.user}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                            error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <g:form resource="${this.user}" method="POST">
            <div class="card-body">
                <div class="input-group required">
                    <label class="col-sm-4" for="username">Nom
                        <span class="required-indicator">*</span>
                    </label><input type="text" name="username" value="${user.username}" required=""
                                   class="form-control col-sm-6"
                                   id="username">
                </div>
                 <hr>
                <div class="input-group" style="margin-bottom: 15px;">
                    <label class="col-sm-4">Role
                        <span class="required-indicator">*</span>
                    </label>
                    <g:select id="role" class="form-control col-sm-6" required="" name="role.id"
                              from="${rolelist}"
                              optionKey="id" optionValue="authority"/>
                </div>
                <hr>
                <div class="input-group">
                    <label class="col-sm-4" for="password">Nouveau mot de passe
                        <span class="required-indicator">*</span>
                    </label><input type="password" name="password" required=""
                                   class="form-control col-sm-6"
                                   value=""
                                   id="password">
                </div>
                <br>

                <div class="input-group">
                    <label class="col-sm-4" for="password">Confirmation mot de passe
                        <span class="required-indicator">*</span>
                    </label><input type="password"  required=""
                                   class="form-control col-sm-6" name="conf_password"
                                   value="" id="confirm_password">

                </div>
                <small id='message' class="offset-4"></small>
                <hr>
                <fieldset class="buttons">
                    <g:submitButton name="create" id="btnModifMdp" class="btn btn-success"
                                    value="Ajouter"/>
                </fieldset>
            </div>
        </g:form>
    </div>
</div>
</body>
</html>
