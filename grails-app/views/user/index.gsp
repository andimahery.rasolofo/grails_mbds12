<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="disposition" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title>Listes des utilisateurs</title>
    </head>
    <body>
    <div class="container-fluid">
        <!-- Page Heading    <h1 class="h3 mb-2 text-gray-800">Gestion des Utilisateurs</h1> -->
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                <h6 class="m-0 font-weight-bold col-sm-10">Liste des utilisateurs</h6>
                    <a href="create" class="btn btn-success btn-sm col-sm-1">
                        Ajouter  <i class="fas fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered"  width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${userList}" var="user">
                            <tr>
                                <th><a href="show/${user.id}" class="text-info">${user.username}</a></th>
                                <th><g:each in="${user.getAuthorities()}" var="role">
                                    ${role.authority}
                                </g:each></th>
                                <th><a href="edit/${user.id}" class="btn btn-info">
                                    <i class="fas fa-edit"></i>
                                </a> <a data-toggle="modal" data-target="#supprimer${user.id}" class="btn btn-danger">
                                    <i class="fas fa-trash"></i>
                                </a></th>
                            </tr>
                            <div class="modal fade" id="supprimer${user.id}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Voulez vous vraiment supprimer cet utilisateur ?</div>
                                        <div class="modal-footer">
                                            <form action="/projet/user/delete/${user.id}" method="post"><input type="hidden" name="_method" value="DELETE" id="_method">
                                                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                                                <input type="submit" class="btn btn-danger" value="Oui">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </g:each>
                        </tbody>
                    </table>

                </div>
                <g:paginate total="${userCount ?: 0}" />
            </div>
        </div>

    </div>

    </body>
</html>