<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="disposition" />
        <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
        <title>Détail de l'utilisateur</title>
    </head>
    <body>
    <div class="container-fluid">
        <!-- Page Heading  <h1 class="h3 mb-2 text-gray-800">Detail de l'annonce</h1>  -->

        <!-- DataTales Example -->
        <g:if test="${flash.message}">
            <div class="alert alert-success col-lg-5" role="status">${flash.message} &nbsp; &nbsp; &nbsp;<asset:image src="checked.png" class="float-right"></asset:image></div>
        </g:if>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-truncate">Détail de l'utilisateur</h6>
            </div>
            <div class="card-body">
                <div class="row" style="margin-bottom: 15px;">
                    <label class="col-sm-4 text-info">Nom d'utilisateur :</label>
                    <label class="col-sm-4">${user.username}</label>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <label class="col-sm-4 text-info">Role :</label>
                    <label class="col-sm-4">${userRole.role.authority}</label>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <label class="col-sm-4 text-info">Mot de passe expiré :</label>
                    <label class="col-sm-4">${user.passwordExpired}</label>
                </div>

                <div class="row" style="margin-bottom: 15px;">
                    <label class="col-sm-4 text-info">Compte activé  :</label>
                    <label class="col-sm-4">${user.enabled}</label>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <label class="col-sm-4 text-info">Compte bloqué  :</label>
                    <label class="col-sm-4">${user.accountLocked}</label>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <label class="col-sm-4 text-info">Compte expiré  :</label>
                    <label class="col-sm-4">${user.accountExpired}</label>
                </div>
                <div class="row">
                    <label class="col-sm-7 text-info" for="annonces">Liste des Annonces :</label>
                </div>
                <br>

                <div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr class="info">
                            <th>Titre</th>
                            <th>Description</th>
                            <th>Prix</th>
                            <th>Illustrations</th>
                            <th>Auteur</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${annonceList}" var="annonce">
                            <tr>
                                <th>${annonce.title}</th>
                                <th>${annonce.description}</th>
                                <th>${annonce.price}</th>
                                <th>
                                    <g:each in="${annonce.illustrations}" var="illustration">
                                        <img src="${grailsApplication.config.annonces.illustrations.url + illustration.filename}"  class="img-thumbnail" width="100" height="100"/>
                                    </g:each>
                                </th>
                                <th>${annonce.author.username}</th>
                                <th><a href="/projet/annonce/show/${annonce.id}" class="btn btn-outline-warning">
                                    Voir détail</a></th>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
                </br>
                <div>
                <a href="/projet/user/edit/${user.id}" class="btn btn-info btn-sm">
                    <i class="fas fa-edit"></i>&nbsp;Modifier</a>
                <a  class="btn btn-danger btn-sm" data-toggle="modal" data-target="#supprimer_annonce">
                    <i class="fas fa-trash"></i>&nbsp;Supprimer</a>
                </div>
                <div class="modal fade" id="supprimer_annonce" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">Voulez vous vraiment supprimer cet utilisateur ?</div>
                            <div class="modal-footer">
                                <form action="/projet/user/delete/${user.id}" method="post"><input type="hidden" name="_method" value="DELETE" id="_method">
                                    <button class="btn btn-light" type="button" data-dismiss="modal">Annuler</button>
                                    <input type="submit" class="btn btn-danger" value="Oui">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>
