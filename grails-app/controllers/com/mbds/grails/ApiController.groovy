package com.mbds.grails

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured
import javassist.NotFoundException
import org.bouncycastle.openssl.PasswordException

import javax.management.InstanceAlreadyExistsException
import javax.servlet.http.HttpServletResponse

@Secured('ROLE_ADMIN')
class ApiController {

    def userService
    def annonceService

//    GET / PUT / PATCH / DELETE
//    url : localhost:8081/projet/api/annonce(s)/{id}
    def annonce() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST
                def annonceInstance = Annonce.get(params.id)
                if (!annonceInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                response.withFormat {
                    json { render annonceInstance as JSON }
                    xml { render annonceInstance as XML }
                }
//                serializeData(annonceInstance, request.getHeader("Accept"))
                break

            case "PUT":
                def params = request.JSON
                if (!params.id || !params.price || !params.author.id || !params.description || !params.title)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                def annonce = annonceService.patch(params, null)

                if (!annonce)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return response.status = HttpServletResponse.SC_OK
                break

            case "PATCH":
                def params = request.JSON
                if (!params.id && (params.price || params.author.id || params.description || params.title))
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                def annonce = annonceService.patch(params, null)

                if (!annonce)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return response.status = HttpServletResponse.SC_OK
                break

            case "DELETE":
                def params = request.JSON
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                try {
                    annonceService.delete(params.id)
                } catch (NotFoundException e) {
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                }

                return response.status = HttpServletResponse.SC_OK
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / POST
    def annonces() {
        switch (request.getMethod()) {
            case "GET":
//                params.max = Math.min(params.max ?: 10, 100)
//                def map = [list: annonceService.list(params), max: params.max, totalCounts: annonceService.count(),
//                           offset: params.offset ?: 0]

                params.page = params.page ? Integer.parseInt(params.page) : 1
                params.max = Math.min(params.max ?: 10, 100)
                params.offset = params.offset ?: (params.page - 1) * params.max
                def count = annonceService.count()
                def lastPage = (int) Math.ceil(count / params.max)
                def nextPage = params.page == lastPage ? null : params.page + 1
                def previousPage = params.page == 1 ? null : params.page - 1
                def map = [list    : annonceService.list(params), max: params.max, totalCounts: count,
                           offset  : params.offset ?: 0, page: params.page, lastPage: lastPage,
                           nextPage: nextPage, previousPage: previousPage]

                response.withFormat {
                    json { render map as JSON }
                    xml { render map as XML }
                }
                break

            case "POST":
                def params = request.JSON
                if (!params.price || !params.author.id || !params.description || !params.title)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                def annonce = annonceService.save(params, null)

                if (!annonce)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return response.status = HttpServletResponse.SC_CREATED
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

//    GET / PUT / PATCH / DELETE
    //    url : localhost:8081/projet/api/user(s)/{id}
    def user() {
        switch (request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST //400
                def userInstance = User.get(params.id)
                def map = [id             : userInstance.id,
                           username       : userInstance.username,
                           passwordExpired: userInstance.passwordExpired,
                           accountLocked  : userInstance.accountLocked,
                           accountExpired : userInstance.accountExpired,
                           enabled        : userInstance.enabled,
                           role           : userInstance.getAuthorities()]
                if (!userInstance)
                    return response.status = HttpServletResponse.SC_NOT_FOUND //404
                response.withFormat {
                    json { render map as JSON }
                    xml { render map as XML }
                }
                serializeData(userInstance, request.getHeader("Accept"))
                break

            case "PUT":
                def params = request.JSON
                if (!params.id || !params.username || (!params.password && params.conf_password) || (params.password && !params.conf_password)
                        || params.enabled == null || params.accountExpired == null || params.accountLocked == null
                        || params.passwordExpired == null || !params.role.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                def user = null
                try {
                    user = userService.patch(params)
                } catch (PasswordException | InstanceAlreadyExistsException e) {
                    def map = [error: e.getMessage()]
                    render {
                        json { render map as JSON }
                        xml { render map as XML }
                    }
                }

                if (!user)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return response.status = HttpServletResponse.SC_OK
                break

            case "PATCH":
                def params = request.JSON
                if (!params.id && (!params.username || (!params.password && params.conf_password) || (params.password && !params.conf_password)
                        || params.enabled == null || params.accountExpired == null || params.accountLocked == null
                        || params.passwordExpired == null || !params.role.id))
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                def user = userService.patch(params)

                if (!user)
                    return response.status = HttpServletResponse.SC_NOT_FOUND

                return response.status = HttpServletResponse.SC_OK
                break

            case "DELETE":
                def params = request.JSON
                if (!params.id)
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                try {
                    userService.delete(params.id)
                } catch (NotFoundException e) {
                    return response.status = HttpServletResponse.SC_NOT_FOUND
                }

                return response.status = HttpServletResponse.SC_OK //200
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED //405
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE //406

    }

//    GET / POST
    def users() {
        switch (request.getMethod()) {
            case "GET":
                params.page = params.page ? Integer.parseInt(params.page) : 1
                params.max = Math.min(params.max ?: 10, 100)
                params.offset = params.offset ?: (params.page - 1) * params.max
                def count = userService.count()
                def lastPage = (int) Math.ceil(count / params.max)
                def nextPage = params.page == lastPage ? null : params.page + 1
                def previousPage = params.page == 1 ? null : params.page - 1
                def list = userService.list(params)
                list.each {
                    it.password = null
                }
                def map = [list    : list, max: params.max, totalCounts: count,
                           offset  : params.offset ?: 0, page: params.page, lastPage: lastPage,
                           nextPage: nextPage, previousPage: previousPage]

                response.withFormat {
                    json { render map as JSON }
                    xml { render map as XML }
                }
                break

            case "POST":
                def params = request.JSON
                if (!params.username || !params.password || !params.conf_password || (!params.role && !params.role.id))
                    return response.status = HttpServletResponse.SC_BAD_REQUEST

                try {
                    userService.save(params)
                } catch (PasswordException | InstanceAlreadyExistsException e) {
                    def map = [error: e.getMessage()]
                    render {
                        json { render map as JSON }
                        xml { render map as XML }
                    }
                }
                return response.status = HttpServletResponse.SC_CREATED
                break
            default:
                return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                break
        }
        return response.status = HttpServletResponse.SC_NOT_ACCEPTABLE
    }

    def serializeData(object, format) {
        switch (format) {
            case 'json':
                render object as JSON
                break
            case 'application/json':
                render object as JSON
                break
            case 'text/json':
                render object as JSON
                break
            case 'xml':
                render object as XML
                break
            case 'application/xml':
                render object as XML
                break
            case 'text/xml':
                render object as XML
                break
        }
    }
}
