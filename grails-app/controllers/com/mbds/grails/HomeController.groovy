package com.mbds.grails

import grails.plugin.springsecurity.annotation.Secured

class HomeController {

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def index() {
        redirect(controller: 'annonce', action: 'index')
    }
}
