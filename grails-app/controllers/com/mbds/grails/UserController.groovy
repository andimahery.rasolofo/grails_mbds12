package com.mbds.grails

import org.bouncycastle.openssl.PasswordException
import org.springframework.security.access.annotation.Secured

import javax.xml.bind.ValidationException

import static org.springframework.http.HttpStatus.*

class UserController {

    UserService userService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond userService.list(params), model: [userCount: userService.count()]
    }

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def show(Long id) {
        def user = userService.get(id)
        respond user, model: [annonceList: Annonce.findAllByAuthor(user), userRole: UserRole.findByUser(user)]
    }

    @Secured(["ROLE_ADMIN"])
    def create() {
        respond new User(params), model: [rolelist: Role.findAll()]
    }

    @Secured(["ROLE_ADMIN"])
    def save() {
        //render(params)
        def user = null
        try {
            user = userService.save(params)
            if (user == null) {
                notFound()
                return
            }
        } catch (ValidationException e) {
            respond user.errors, view: 'create'
            return
        } catch (PasswordException exp) {
            respond exp.getMessage(), view: 'create'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = "Utilisateur créée avec succès "
                redirect user
            }
            '*' { respond user, [status: CREATED] }
        }
    }

    @Secured(["ROLE_ADMIN"])
    def edit(Long id) {
        def user = userService.get(id)
        respond user, model: [annonceList: Annonce.findAllByAuthor(user), rolelist: Role.findAll(), userRole: UserRole.findByUser(user)]
    }

//    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    @Secured(["ROLE_ADMIN"])
    def update() {
        def user = null
        try {
            user = userService.update(params)
            if (user == null) {
                notFound()
                return
            }
        } catch (Exception ex) {
            redirect(action: "edit", id: params.id)
            flash.error = ex.getMessage()
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = "Utilisateur modifié avec succès "
                redirect user
            }
            '*' { respond user, [status: OK] }
        }
    }

    @Secured(["ROLE_ADMIN"])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        userService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = "Utilisateur supprimé avec succès"
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
