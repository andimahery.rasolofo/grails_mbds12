package com.mbds.grails


import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.*

class AnnonceController {

    AnnonceService annonceService

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond annonceService.list(params), model: [annonceCount: annonceService.count()]
    }

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def show(Long id) {
        respond annonceService.get(id)
    }

    @Secured(["ROLE_ADMIN"])
    def create() {
        respond new Annonce(params), model: [userList: User.findAll()]
    }

    @Secured(["ROLE_ADMIN"])
    def save() {
        def annonce = null
        try {
            annonce = annonceService.save(params, request.getFiles('files[]'))
            if (annonce == null) {
                notFound()
                return
            }
        } catch (ValidationException e) {
            respond annonce.errors, view: 'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = "Annonce créée avec succès"
                redirect annonce
            }
            '*' { respond annonce, [status: CREATED] }
        }
    }

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def edit(Long id) {
        respond annonceService.get(id), model: [userList: User.list(), baseUrl: grailsApplication.config.annonces.illustrations.url]
    }

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    def update() {
        def annonce = null
        try {
            annonce = annonceService.update(params, request.getFiles('files[]'))
//            annonce = annonceService.update(params, request.getFile('file'))
            if (annonce == null) {
                notFound()
                return
            }
        } catch (ValidationException e) {
            respond annonce.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = "Annonce modifiée avec succès"
                redirect annonce
            }
            '*' { respond annonce, [status: OK] }
        }
    }

    @Secured(["ROLE_ADMIN"])
    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        annonceService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = "Annonce supprimée avec succès"
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Secured(["ROLE_MODO", "ROLE_ADMIN"])
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'annonce.label', default: 'Annonce'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
