**Projet** : Coin coin Admin
- Une application web pour administrer les annonces d'une entreprise appelée "Lecoincoin". Il y aura trois types d'utilisateurs pour l'application : l'administrateur, le modérateur et le client.

**Technologies utilisés** :
  - Grails 3.3.8
  - JQuery 
  - API REST
  - Spring security

**Fonctionnalités** :
   - Authentification de l'administrateur / modérateur
   - Création / Visualisation / Mise à jour / Suppression pour :
      - Les utilisateurs
      - Les annonces
   - Ajout et Suppression multiple des illustrations d'une annonce 
   - Recherche pour filtrer les annonces d'un utilisateur

**Auteurs** : Groupe N° 12
   - ANDRIAMBAHOAKA Mahery Joela N°4
   - RASOLOFO Andimahery Loic N°48
    


